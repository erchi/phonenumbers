function format(nr2) {
  let nr2copy = nr2;
  if (nr2copy.startsWith("00")) {
    nr2copy = nr2copy.replace("00", "+").replace(/[\s(/)-]/g, "");
  } else if (nr2copy.startsWith("0")) {
    nr2copy = nr2copy.replace("0", "+421").replace(/[\s(/)-]/g, "");
  } else if (nr2copy.startsWith("421")) {
    nr2copy = nr2copy.replace("421", "+421").replace(/[\s(/)-]/g, "");
  } else if (nr2copy.startsWith("+421")) {
    nr2copy = nr2copy.replace(/[\s(/)-]/g, "");
  } else if (nr2copy.match(/[1-9]/)) {
    nr2copy = nr2copy.replace(/[\s(/)-]/g, "");
  }
  return nr2copy;
}
function phoneNrsAreEqual(nr1, nr2) {
  const formatednr = format(nr2);
  return !!check(nr1, formatednr);
}

function check(nr1, nr2) {
  return nr1 === nr2;
}

module.exports = phoneNrsAreEqual;
