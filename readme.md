**Phone Numbers Jest Test**  
Testing various telephone number formats.

**Getting Started**  
1. npm install

**Running the tests**  
2. npm run test

**Built With**  
Jest - is a JavaScript testing framework designed to ensure correctness of any JavaScript codebase.
