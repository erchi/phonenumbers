const phoneNrsAreEqual = require("./Function");

test("clasic telephone number to equal to number with spaces", () => {
  expect(phoneNrsAreEqual("+421944923459", "+421 944 923 459")).toBe(true);
});

test("clasic telephone number to equal to number without country code", () => {
  expect(phoneNrsAreEqual("+421944923459", "0944923459")).toBe(true);
});

test("clasic telephone number to equal to number with double zero and dashes", () => {
  expect(phoneNrsAreEqual("+421944923459", "00/(421)-944-92/3-459")).toBe(true);
});

test("clasic telephone number to equal to number with double zero", () => {
  expect(phoneNrsAreEqual("+421944923459", "00421944923459")).toBe(true);
});

test("clasic telephone number to equal to number without country code, with slash", () => {
  expect(phoneNrsAreEqual("+421944923459", "0944/923459")).toBe(true);
});

test("clasic telephone number to equal to number with double zero, slash and dashes", () => {
  expect(phoneNrsAreEqual("+421944923459", "00421/944-923-459")).toBe(true);
});

test("clasic telephone number to equal to number with double zero, dashes, braces, slash", () => {
  expect(phoneNrsAreEqual("+421944923459", "00(421)/9-44 92-3459")).toBe(true);
});

test("clasic telephone number to equal to number from abroad with spaces", () => {
  expect(phoneNrsAreEqual("1944923459", "1944 923 459")).toBe(true);
});

test("clasic telephone number to equal to number with spaces and without plus", () => {
  expect(phoneNrsAreEqual("+421944923459", "421 944 923 459")).toBe(true);
});

test("clasic telephone number to equal to number with country code, spaces and braces", () => {
  expect(phoneNrsAreEqual("+421944923459", "+421 (944) 923 459")).toBe(true);
});

test("clasic telephone number to equal to number with braces, spaces but without country code", () => {
  expect(phoneNrsAreEqual("+421944923459", "0 (944) 923 459")).toBe(true);
});

test("clasic telephone number to equal to number with country code, spaces, dash, braces, without plus", () => {
  expect(phoneNrsAreEqual("+421944923459", "421 (944)-923 459")).toBe(true);
});

test("clasic telephone number to equal to number with dashes and braces", () => {
  expect(phoneNrsAreEqual("+421944923459", "+421-(944)-923-459")).toBe(true);
});

test("clasic telephone number to equal to number with braces(error because of dots)", () => {
  expect(phoneNrsAreEqual("+421944923459", "0 (9.4.4). 923. 4.5.9")).toBe(true);
});

test("clasic telephone number to equal to number with country code, spaces, dash, braces(error because of letters)", () => {
  expect(phoneNrsAreEqual("+421944923459", "421 (9df44)-9a23 459")).toBe(true);
});

test("clasic telephone number to equal to number without country code and spaces(error because of | sign)", () => {
  expect(phoneNrsAreEqual("+421944923459", "+421-(9|44)-92|3-45|9")).toBe(true);
});

test("clasic telephone number to equal to number without country code and spaces", () => {
  expect(phoneNrsAreEqual("+421944923459", "+421-(944)-923-459")).toBe(true);
});

test("clasic telephone number to equal to number from abroad with dashes(error because this number is not in the DB)", () => {
  expect(phoneNrsAreEqual("+421944923459", "1 234-567-898")).toBe(true);
});

test("clasic telephone number to equal to number from abroad with dashes(error because this number is not in the DB)", () => {
  expect(phoneNrsAreEqual("+421944923459", "4(657)463-524")).toBe(true);
});

test("clasic telephone number to equal to number from abroad(error because of not allowed signs)", () => {
  expect(phoneNrsAreEqual("+421944923459", "5-(6s74)/8p29-5c46")).toBe(true);
});
